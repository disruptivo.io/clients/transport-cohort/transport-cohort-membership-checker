### 0.2.0 (2023-10-03)

##### Chores

*  update transport-cohort-db submodule (a714925f)
*  update transport-cohort-db submodule (532b5c48)

##### Documentation Changes

*  add info to run the project (ea998451)

##### New Features

*  use tanstack-query in place of swr (fd70038d)
*  generate tanstack-query hooks (7e443014)

##### Other Changes

*  add info about the env file (e4e19fe3)

#### 0.1.3 (2023-08-16)

##### New Features

*  fix error for affiliate detail route (2f803654)
*  add error for affiliate detail route (52373bba)

#### 0.1.2 (2023-08-16)

##### New Features

*  add global error (3f193540)

##### Other Changes

*  remove react query dependency (6a1d7346)

#### 0.1.1 (2023-08-16)

##### New Features

*  add version to root layout (ed875b82)

### 0.1.0 (2023-08-16)

##### Chores

*  change favicon (bb43e145)
*  add new favicon (28944c62)
*  add ramda (b03d0dbf)
*  remove debug logs (a00169a9)
*  add debug logs (fbd5bf82)
*  update pm2 config file (097a093d)
*  update pm2 config file (36e3bebe)
*  update pm2 config file (3524e058)
*  change app path on pm2 config file (ac36a472)
*  update api url on pm2 config file (1ae9e6b8)
*  update path for pm2 config (fab5c35b)
*  update path for pm2 config (9794ee2e)
*  update path for pm2 config (fe8775ff)
*  add port env var to pm2 config (53b420d7)
*  update git submodule (c2ba3cf0)
*  update git submodule (fc8db3b3)
*  update pre-deploy hook on pm2 config (d899399f)
*  add scripts related to pm2 (6feb8341)
*  add pm2 config file (50e326f0)
*  install pm2 (7c97628c)
*  install react toastify (e949526a)
*  install moment (4fb0949e)
*  configure tailwind forms (1cb279ed)
*  install tailwind forms and sass (8b7c080d)
*  update git submodule (6ebd2ee6)
*  install zenstack dependencies (f5dd3573)
*  update git submodule (e0ce3e19)
*  update git submodule (2a90474d)
*  update git submodule (2ff41cbb)
*  add git submodule (3ff52dd7)
*  pin node and yarn version (a45c7454)

##### Documentation Changes

*  add .env.example file (31cfcc7a)
*  add info for adding env vars (4063ae24)
*  add info for deploying using pm2 (a853f3ff)

##### New Features

*  add and config changelog generator (a05a8671)
*  add colors to tailwind config (ea747ae8)
*  add logo to main page (f4300e73)
*  add logo component (4e829a06)
*  omit fields using ramda (f0f169b7)
*  change tanstack query by swr (85022b94)
*  add toast to search component (b401c519)
*  configure react toastify (65aa7949)
*  update affiliate detail page and layout (09ee9f37)
*  add error component (daefd055)
*  add custom minWidth for tailwind (0767b33d)
*  display search status on Search component (182a8263)
*  create spinner component (f2aafb8e)
*  add close and check icons (beeb7138)
*  use scss instead of css (65d7af7e)
*  add Search component to Home page (9b6d92eb)
*  create Search component (080b1931)
*  add show props to Spinner component (fa79718d)
*  create Spinner component (4ae785b3)
*  add Providers to RootLayout (5c097823)
*  create Providers component (3c4fbab4)
*  add user detail page (2cffdb35)
*  update user finder page (8621e8f1)

##### Bug Fixes

*  change label of search component (2ee67e2d)
*  change user header title (8e8c5dc6)
*  load api url from env var (b20fa125)
*  harcode api url (c0d14935)
*  add debug log to root layout (b0046beb)
*  add debug logs on providers component (56c80e45)
*  fix logic for displaying the close and check icons (e92a8f7f)
*  fix typescript issues on search component (c0ce4dda)
*  convert to string the endpoint value to avoid issues (8328fa2b)
*  add missing type to affiliate detail page (760840f6)
*  add missing key to affiliate detail layout (d03870da)
*  translate english labels displayed on affiliate detail view (47175eab)
*  remove htlm and body tag from affiliate detail layout (83327399)
*  import right global styles on root layout component (08ca7b15)

##### Other Changes

*  remove timeout code (933bb0e6)
*  update pre-deploy hook on pm2 config" (b4bc0cb7)

##### Code Style Changes

*  update logo style (a2ec3be6)
*  change colors (5af9190d)
*  improve icon styles (7a1b4c7d)
*  change colors (de990e72)
*  update style of search component (bfb2ac8a)
*  update style of root page (19b31f04)
*  update style of root layout (9a8619e4)
*  add global style for inputs and buttons (e44fa91e)

