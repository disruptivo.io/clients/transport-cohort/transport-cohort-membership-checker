/* eslint-disable */
import type { Prisma, Enterprise } from '@prisma/client';
import { useContext } from 'react';
import type { UseMutationOptions, UseQueryOptions, UseInfiniteQueryOptions } from '@tanstack/react-query';
import { RequestHandlerContext } from '@zenstackhq/tanstack-query/runtime/react';
import {
    query,
    infiniteQuery,
    postMutation,
    putMutation,
    deleteMutation,
} from '@zenstackhq/tanstack-query/runtime/react';
import type { PickEnumerable, CheckSelect } from '@zenstackhq/tanstack-query/runtime';

export function useCreateEnterprise(
    options?: Omit<UseMutationOptions<Enterprise | undefined, unknown, Prisma.EnterpriseCreateArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = postMutation<Prisma.EnterpriseCreateArgs, Enterprise, true>(
        'Enterprise',
        `${endpoint}/enterprise/create`,
        options,
        fetch,
        invalidateQueries,
        true,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.EnterpriseCreateArgs>(
            args: Prisma.SelectSubset<T, Prisma.EnterpriseCreateArgs>,
            options?: Omit<
                UseMutationOptions<
                    CheckSelect<T, Enterprise, Prisma.EnterpriseGetPayload<T>> | undefined,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.EnterpriseCreateArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as
                | CheckSelect<T, Enterprise, Prisma.EnterpriseGetPayload<T>>
                | undefined;
        },
    };
    return mutation;
}

export function useFindManyEnterprise<T extends Prisma.EnterpriseFindManyArgs>(
    args?: Prisma.SelectSubset<T, Prisma.EnterpriseFindManyArgs>,
    options?: UseQueryOptions<Array<Prisma.EnterpriseGetPayload<T>>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<Array<Prisma.EnterpriseGetPayload<T>>>(
        'Enterprise',
        `${endpoint}/enterprise/findMany`,
        args,
        options,
        fetch,
    );
}

export function useInfiniteFindManyEnterprise<T extends Prisma.EnterpriseFindManyArgs>(
    args?: Prisma.SelectSubset<T, Prisma.EnterpriseFindManyArgs>,
    options?: UseInfiniteQueryOptions<Array<Prisma.EnterpriseGetPayload<T>>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return infiniteQuery<Array<Prisma.EnterpriseGetPayload<T>>>(
        'Enterprise',
        `${endpoint}/enterprise/findMany`,
        args,
        options,
        fetch,
    );
}

export function useFindUniqueEnterprise<T extends Prisma.EnterpriseFindUniqueArgs>(
    args: Prisma.SelectSubset<T, Prisma.EnterpriseFindUniqueArgs>,
    options?: UseQueryOptions<Prisma.EnterpriseGetPayload<T>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<Prisma.EnterpriseGetPayload<T>>(
        'Enterprise',
        `${endpoint}/enterprise/findUnique`,
        args,
        options,
        fetch,
    );
}

export function useFindFirstEnterprise<T extends Prisma.EnterpriseFindFirstArgs>(
    args?: Prisma.SelectSubset<T, Prisma.EnterpriseFindFirstArgs>,
    options?: UseQueryOptions<Prisma.EnterpriseGetPayload<T>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<Prisma.EnterpriseGetPayload<T>>(
        'Enterprise',
        `${endpoint}/enterprise/findFirst`,
        args,
        options,
        fetch,
    );
}

export function useUpdateEnterprise(
    options?: Omit<UseMutationOptions<Enterprise | undefined, unknown, Prisma.EnterpriseUpdateArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = putMutation<Prisma.EnterpriseUpdateArgs, Enterprise, true>(
        'Enterprise',
        `${endpoint}/enterprise/update`,
        options,
        fetch,
        invalidateQueries,
        true,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.EnterpriseUpdateArgs>(
            args: Prisma.SelectSubset<T, Prisma.EnterpriseUpdateArgs>,
            options?: Omit<
                UseMutationOptions<
                    CheckSelect<T, Enterprise, Prisma.EnterpriseGetPayload<T>> | undefined,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.EnterpriseUpdateArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as
                | CheckSelect<T, Enterprise, Prisma.EnterpriseGetPayload<T>>
                | undefined;
        },
    };
    return mutation;
}

export function useUpdateManyEnterprise(
    options?: Omit<UseMutationOptions<Prisma.BatchPayload, unknown, Prisma.EnterpriseUpdateManyArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = putMutation<Prisma.EnterpriseUpdateManyArgs, Prisma.BatchPayload, false>(
        'Enterprise',
        `${endpoint}/enterprise/updateMany`,
        options,
        fetch,
        invalidateQueries,
        false,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.EnterpriseUpdateManyArgs>(
            args: Prisma.SelectSubset<T, Prisma.EnterpriseUpdateManyArgs>,
            options?: Omit<
                UseMutationOptions<
                    Prisma.BatchPayload,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.EnterpriseUpdateManyArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as Prisma.BatchPayload;
        },
    };
    return mutation;
}

export function useUpsertEnterprise(
    options?: Omit<UseMutationOptions<Enterprise | undefined, unknown, Prisma.EnterpriseUpsertArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = postMutation<Prisma.EnterpriseUpsertArgs, Enterprise, true>(
        'Enterprise',
        `${endpoint}/enterprise/upsert`,
        options,
        fetch,
        invalidateQueries,
        true,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.EnterpriseUpsertArgs>(
            args: Prisma.SelectSubset<T, Prisma.EnterpriseUpsertArgs>,
            options?: Omit<
                UseMutationOptions<
                    CheckSelect<T, Enterprise, Prisma.EnterpriseGetPayload<T>> | undefined,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.EnterpriseUpsertArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as
                | CheckSelect<T, Enterprise, Prisma.EnterpriseGetPayload<T>>
                | undefined;
        },
    };
    return mutation;
}

export function useDeleteEnterprise(
    options?: Omit<UseMutationOptions<Enterprise | undefined, unknown, Prisma.EnterpriseDeleteArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = deleteMutation<Prisma.EnterpriseDeleteArgs, Enterprise, true>(
        'Enterprise',
        `${endpoint}/enterprise/delete`,
        options,
        fetch,
        invalidateQueries,
        true,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.EnterpriseDeleteArgs>(
            args: Prisma.SelectSubset<T, Prisma.EnterpriseDeleteArgs>,
            options?: Omit<
                UseMutationOptions<
                    CheckSelect<T, Enterprise, Prisma.EnterpriseGetPayload<T>> | undefined,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.EnterpriseDeleteArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as
                | CheckSelect<T, Enterprise, Prisma.EnterpriseGetPayload<T>>
                | undefined;
        },
    };
    return mutation;
}

export function useDeleteManyEnterprise(
    options?: Omit<UseMutationOptions<Prisma.BatchPayload, unknown, Prisma.EnterpriseDeleteManyArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = deleteMutation<Prisma.EnterpriseDeleteManyArgs, Prisma.BatchPayload, false>(
        'Enterprise',
        `${endpoint}/enterprise/deleteMany`,
        options,
        fetch,
        invalidateQueries,
        false,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.EnterpriseDeleteManyArgs>(
            args: Prisma.SelectSubset<T, Prisma.EnterpriseDeleteManyArgs>,
            options?: Omit<
                UseMutationOptions<
                    Prisma.BatchPayload,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.EnterpriseDeleteManyArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as Prisma.BatchPayload;
        },
    };
    return mutation;
}

export function useAggregateEnterprise<T extends Prisma.EnterpriseAggregateArgs>(
    args: Prisma.SelectSubset<T, Prisma.EnterpriseAggregateArgs>,
    options?: UseQueryOptions<Prisma.GetEnterpriseAggregateType<T>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<Prisma.GetEnterpriseAggregateType<T>>(
        'Enterprise',
        `${endpoint}/enterprise/aggregate`,
        args,
        options,
        fetch,
    );
}

export function useGroupByEnterprise<
    T extends Prisma.EnterpriseGroupByArgs,
    HasSelectOrTake extends Prisma.Or<Prisma.Extends<'skip', Prisma.Keys<T>>, Prisma.Extends<'take', Prisma.Keys<T>>>,
    OrderByArg extends Prisma.True extends HasSelectOrTake
        ? { orderBy: Prisma.EnterpriseGroupByArgs['orderBy'] }
        : { orderBy?: Prisma.EnterpriseGroupByArgs['orderBy'] },
    OrderFields extends Prisma.ExcludeUnderscoreKeys<Prisma.Keys<Prisma.MaybeTupleToUnion<T['orderBy']>>>,
    ByFields extends Prisma.MaybeTupleToUnion<T['by']>,
    ByValid extends Prisma.Has<ByFields, OrderFields>,
    HavingFields extends Prisma.GetHavingFields<T['having']>,
    HavingValid extends Prisma.Has<ByFields, HavingFields>,
    ByEmpty extends T['by'] extends never[] ? Prisma.True : Prisma.False,
    InputErrors extends ByEmpty extends Prisma.True
        ? `Error: "by" must not be empty.`
        : HavingValid extends Prisma.False
        ? {
              [P in HavingFields]: P extends ByFields
                  ? never
                  : P extends string
                  ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
                  : [Error, 'Field ', P, ` in "having" needs to be provided in "by"`];
          }[HavingFields]
        : 'take' extends Prisma.Keys<T>
        ? 'orderBy' extends Prisma.Keys<T>
            ? ByValid extends Prisma.True
                ? {}
                : {
                      [P in OrderFields]: P extends ByFields
                          ? never
                          : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`;
                  }[OrderFields]
            : 'Error: If you provide "take", you also need to provide "orderBy"'
        : 'skip' extends Prisma.Keys<T>
        ? 'orderBy' extends Prisma.Keys<T>
            ? ByValid extends Prisma.True
                ? {}
                : {
                      [P in OrderFields]: P extends ByFields
                          ? never
                          : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`;
                  }[OrderFields]
            : 'Error: If you provide "skip", you also need to provide "orderBy"'
        : ByValid extends Prisma.True
        ? {}
        : {
              [P in OrderFields]: P extends ByFields
                  ? never
                  : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`;
          }[OrderFields],
>(
    args: Prisma.SelectSubset<T, Prisma.SubsetIntersection<T, Prisma.EnterpriseGroupByArgs, OrderByArg> & InputErrors>,
    options?: UseQueryOptions<
        {} extends InputErrors
            ? Array<
                  PickEnumerable<Prisma.EnterpriseGroupByOutputType, T['by']> & {
                      [P in keyof T & keyof Prisma.EnterpriseGroupByOutputType]: P extends '_count'
                          ? T[P] extends boolean
                              ? number
                              : Prisma.GetScalarType<T[P], Prisma.EnterpriseGroupByOutputType[P]>
                          : Prisma.GetScalarType<T[P], Prisma.EnterpriseGroupByOutputType[P]>;
                  }
              >
            : InputErrors
    >,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<
        {} extends InputErrors
            ? Array<
                  PickEnumerable<Prisma.EnterpriseGroupByOutputType, T['by']> & {
                      [P in keyof T & keyof Prisma.EnterpriseGroupByOutputType]: P extends '_count'
                          ? T[P] extends boolean
                              ? number
                              : Prisma.GetScalarType<T[P], Prisma.EnterpriseGroupByOutputType[P]>
                          : Prisma.GetScalarType<T[P], Prisma.EnterpriseGroupByOutputType[P]>;
                  }
              >
            : InputErrors
    >('Enterprise', `${endpoint}/enterprise/groupBy`, args, options, fetch);
}

export function useCountEnterprise<T extends Prisma.EnterpriseCountArgs>(
    args?: Prisma.SelectSubset<T, Prisma.EnterpriseCountArgs>,
    options?: UseQueryOptions<
        T extends { select: any }
            ? T['select'] extends true
                ? number
                : Prisma.GetScalarType<T['select'], Prisma.EnterpriseCountAggregateOutputType>
            : number
    >,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<
        T extends { select: any }
            ? T['select'] extends true
                ? number
                : Prisma.GetScalarType<T['select'], Prisma.EnterpriseCountAggregateOutputType>
            : number
    >('Enterprise', `${endpoint}/enterprise/count`, args, options, fetch);
}
