/* eslint-disable */
import type { Prisma, Affiliate } from '@prisma/client';
import { useContext } from 'react';
import type { UseMutationOptions, UseQueryOptions, UseInfiniteQueryOptions } from '@tanstack/react-query';
import { RequestHandlerContext } from '@zenstackhq/tanstack-query/runtime/react';
import {
    query,
    infiniteQuery,
    postMutation,
    putMutation,
    deleteMutation,
} from '@zenstackhq/tanstack-query/runtime/react';
import type { PickEnumerable, CheckSelect } from '@zenstackhq/tanstack-query/runtime';

export function useCreateAffiliate(
    options?: Omit<UseMutationOptions<Affiliate | undefined, unknown, Prisma.AffiliateCreateArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = postMutation<Prisma.AffiliateCreateArgs, Affiliate, true>(
        'Affiliate',
        `${endpoint}/affiliate/create`,
        options,
        fetch,
        invalidateQueries,
        true,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.AffiliateCreateArgs>(
            args: Prisma.SelectSubset<T, Prisma.AffiliateCreateArgs>,
            options?: Omit<
                UseMutationOptions<
                    CheckSelect<T, Affiliate, Prisma.AffiliateGetPayload<T>> | undefined,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.AffiliateCreateArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as
                | CheckSelect<T, Affiliate, Prisma.AffiliateGetPayload<T>>
                | undefined;
        },
    };
    return mutation;
}

export function useFindManyAffiliate<T extends Prisma.AffiliateFindManyArgs>(
    args?: Prisma.SelectSubset<T, Prisma.AffiliateFindManyArgs>,
    options?: UseQueryOptions<Array<Prisma.AffiliateGetPayload<T>>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<Array<Prisma.AffiliateGetPayload<T>>>(
        'Affiliate',
        `${endpoint}/affiliate/findMany`,
        args,
        options,
        fetch,
    );
}

export function useInfiniteFindManyAffiliate<T extends Prisma.AffiliateFindManyArgs>(
    args?: Prisma.SelectSubset<T, Prisma.AffiliateFindManyArgs>,
    options?: UseInfiniteQueryOptions<Array<Prisma.AffiliateGetPayload<T>>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return infiniteQuery<Array<Prisma.AffiliateGetPayload<T>>>(
        'Affiliate',
        `${endpoint}/affiliate/findMany`,
        args,
        options,
        fetch,
    );
}

export function useFindUniqueAffiliate<T extends Prisma.AffiliateFindUniqueArgs>(
    args: Prisma.SelectSubset<T, Prisma.AffiliateFindUniqueArgs>,
    options?: UseQueryOptions<Prisma.AffiliateGetPayload<T>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<Prisma.AffiliateGetPayload<T>>('Affiliate', `${endpoint}/affiliate/findUnique`, args, options, fetch);
}

export function useFindFirstAffiliate<T extends Prisma.AffiliateFindFirstArgs>(
    args?: Prisma.SelectSubset<T, Prisma.AffiliateFindFirstArgs>,
    options?: UseQueryOptions<Prisma.AffiliateGetPayload<T>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<Prisma.AffiliateGetPayload<T>>('Affiliate', `${endpoint}/affiliate/findFirst`, args, options, fetch);
}

export function useUpdateAffiliate(
    options?: Omit<UseMutationOptions<Affiliate | undefined, unknown, Prisma.AffiliateUpdateArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = putMutation<Prisma.AffiliateUpdateArgs, Affiliate, true>(
        'Affiliate',
        `${endpoint}/affiliate/update`,
        options,
        fetch,
        invalidateQueries,
        true,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.AffiliateUpdateArgs>(
            args: Prisma.SelectSubset<T, Prisma.AffiliateUpdateArgs>,
            options?: Omit<
                UseMutationOptions<
                    CheckSelect<T, Affiliate, Prisma.AffiliateGetPayload<T>> | undefined,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.AffiliateUpdateArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as
                | CheckSelect<T, Affiliate, Prisma.AffiliateGetPayload<T>>
                | undefined;
        },
    };
    return mutation;
}

export function useUpdateManyAffiliate(
    options?: Omit<UseMutationOptions<Prisma.BatchPayload, unknown, Prisma.AffiliateUpdateManyArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = putMutation<Prisma.AffiliateUpdateManyArgs, Prisma.BatchPayload, false>(
        'Affiliate',
        `${endpoint}/affiliate/updateMany`,
        options,
        fetch,
        invalidateQueries,
        false,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.AffiliateUpdateManyArgs>(
            args: Prisma.SelectSubset<T, Prisma.AffiliateUpdateManyArgs>,
            options?: Omit<
                UseMutationOptions<
                    Prisma.BatchPayload,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.AffiliateUpdateManyArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as Prisma.BatchPayload;
        },
    };
    return mutation;
}

export function useUpsertAffiliate(
    options?: Omit<UseMutationOptions<Affiliate | undefined, unknown, Prisma.AffiliateUpsertArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = postMutation<Prisma.AffiliateUpsertArgs, Affiliate, true>(
        'Affiliate',
        `${endpoint}/affiliate/upsert`,
        options,
        fetch,
        invalidateQueries,
        true,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.AffiliateUpsertArgs>(
            args: Prisma.SelectSubset<T, Prisma.AffiliateUpsertArgs>,
            options?: Omit<
                UseMutationOptions<
                    CheckSelect<T, Affiliate, Prisma.AffiliateGetPayload<T>> | undefined,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.AffiliateUpsertArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as
                | CheckSelect<T, Affiliate, Prisma.AffiliateGetPayload<T>>
                | undefined;
        },
    };
    return mutation;
}

export function useDeleteAffiliate(
    options?: Omit<UseMutationOptions<Affiliate | undefined, unknown, Prisma.AffiliateDeleteArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = deleteMutation<Prisma.AffiliateDeleteArgs, Affiliate, true>(
        'Affiliate',
        `${endpoint}/affiliate/delete`,
        options,
        fetch,
        invalidateQueries,
        true,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.AffiliateDeleteArgs>(
            args: Prisma.SelectSubset<T, Prisma.AffiliateDeleteArgs>,
            options?: Omit<
                UseMutationOptions<
                    CheckSelect<T, Affiliate, Prisma.AffiliateGetPayload<T>> | undefined,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.AffiliateDeleteArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as
                | CheckSelect<T, Affiliate, Prisma.AffiliateGetPayload<T>>
                | undefined;
        },
    };
    return mutation;
}

export function useDeleteManyAffiliate(
    options?: Omit<UseMutationOptions<Prisma.BatchPayload, unknown, Prisma.AffiliateDeleteManyArgs>, 'mutationFn'>,
    invalidateQueries: boolean = true,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    const _mutation = deleteMutation<Prisma.AffiliateDeleteManyArgs, Prisma.BatchPayload, false>(
        'Affiliate',
        `${endpoint}/affiliate/deleteMany`,
        options,
        fetch,
        invalidateQueries,
        false,
    );
    const mutation = {
        ..._mutation,
        async mutateAsync<T extends Prisma.AffiliateDeleteManyArgs>(
            args: Prisma.SelectSubset<T, Prisma.AffiliateDeleteManyArgs>,
            options?: Omit<
                UseMutationOptions<
                    Prisma.BatchPayload,
                    unknown,
                    Prisma.SelectSubset<T, Prisma.AffiliateDeleteManyArgs>
                >,
                'mutationFn'
            >,
        ) {
            return (await _mutation.mutateAsync(args, options as any)) as Prisma.BatchPayload;
        },
    };
    return mutation;
}

export function useAggregateAffiliate<T extends Prisma.AffiliateAggregateArgs>(
    args: Prisma.SelectSubset<T, Prisma.AffiliateAggregateArgs>,
    options?: UseQueryOptions<Prisma.GetAffiliateAggregateType<T>>,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<Prisma.GetAffiliateAggregateType<T>>(
        'Affiliate',
        `${endpoint}/affiliate/aggregate`,
        args,
        options,
        fetch,
    );
}

export function useGroupByAffiliate<
    T extends Prisma.AffiliateGroupByArgs,
    HasSelectOrTake extends Prisma.Or<Prisma.Extends<'skip', Prisma.Keys<T>>, Prisma.Extends<'take', Prisma.Keys<T>>>,
    OrderByArg extends Prisma.True extends HasSelectOrTake
        ? { orderBy: Prisma.AffiliateGroupByArgs['orderBy'] }
        : { orderBy?: Prisma.AffiliateGroupByArgs['orderBy'] },
    OrderFields extends Prisma.ExcludeUnderscoreKeys<Prisma.Keys<Prisma.MaybeTupleToUnion<T['orderBy']>>>,
    ByFields extends Prisma.MaybeTupleToUnion<T['by']>,
    ByValid extends Prisma.Has<ByFields, OrderFields>,
    HavingFields extends Prisma.GetHavingFields<T['having']>,
    HavingValid extends Prisma.Has<ByFields, HavingFields>,
    ByEmpty extends T['by'] extends never[] ? Prisma.True : Prisma.False,
    InputErrors extends ByEmpty extends Prisma.True
        ? `Error: "by" must not be empty.`
        : HavingValid extends Prisma.False
        ? {
              [P in HavingFields]: P extends ByFields
                  ? never
                  : P extends string
                  ? `Error: Field "${P}" used in "having" needs to be provided in "by".`
                  : [Error, 'Field ', P, ` in "having" needs to be provided in "by"`];
          }[HavingFields]
        : 'take' extends Prisma.Keys<T>
        ? 'orderBy' extends Prisma.Keys<T>
            ? ByValid extends Prisma.True
                ? {}
                : {
                      [P in OrderFields]: P extends ByFields
                          ? never
                          : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`;
                  }[OrderFields]
            : 'Error: If you provide "take", you also need to provide "orderBy"'
        : 'skip' extends Prisma.Keys<T>
        ? 'orderBy' extends Prisma.Keys<T>
            ? ByValid extends Prisma.True
                ? {}
                : {
                      [P in OrderFields]: P extends ByFields
                          ? never
                          : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`;
                  }[OrderFields]
            : 'Error: If you provide "skip", you also need to provide "orderBy"'
        : ByValid extends Prisma.True
        ? {}
        : {
              [P in OrderFields]: P extends ByFields
                  ? never
                  : `Error: Field "${P}" in "orderBy" needs to be provided in "by"`;
          }[OrderFields],
>(
    args: Prisma.SelectSubset<T, Prisma.SubsetIntersection<T, Prisma.AffiliateGroupByArgs, OrderByArg> & InputErrors>,
    options?: UseQueryOptions<
        {} extends InputErrors
            ? Array<
                  PickEnumerable<Prisma.AffiliateGroupByOutputType, T['by']> & {
                      [P in keyof T & keyof Prisma.AffiliateGroupByOutputType]: P extends '_count'
                          ? T[P] extends boolean
                              ? number
                              : Prisma.GetScalarType<T[P], Prisma.AffiliateGroupByOutputType[P]>
                          : Prisma.GetScalarType<T[P], Prisma.AffiliateGroupByOutputType[P]>;
                  }
              >
            : InputErrors
    >,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<
        {} extends InputErrors
            ? Array<
                  PickEnumerable<Prisma.AffiliateGroupByOutputType, T['by']> & {
                      [P in keyof T & keyof Prisma.AffiliateGroupByOutputType]: P extends '_count'
                          ? T[P] extends boolean
                              ? number
                              : Prisma.GetScalarType<T[P], Prisma.AffiliateGroupByOutputType[P]>
                          : Prisma.GetScalarType<T[P], Prisma.AffiliateGroupByOutputType[P]>;
                  }
              >
            : InputErrors
    >('Affiliate', `${endpoint}/affiliate/groupBy`, args, options, fetch);
}

export function useCountAffiliate<T extends Prisma.AffiliateCountArgs>(
    args?: Prisma.SelectSubset<T, Prisma.AffiliateCountArgs>,
    options?: UseQueryOptions<
        T extends { select: any }
            ? T['select'] extends true
                ? number
                : Prisma.GetScalarType<T['select'], Prisma.AffiliateCountAggregateOutputType>
            : number
    >,
) {
    const { endpoint, fetch } = useContext(RequestHandlerContext);
    return query<
        T extends { select: any }
            ? T['select'] extends true
                ? number
                : Prisma.GetScalarType<T['select'], Prisma.AffiliateCountAggregateOutputType>
            : number
    >('Affiliate', `${endpoint}/affiliate/count`, args, options, fetch);
}
