module.exports = {
  apps : [
    {
      name   : 'transport-cohort-membership-checker',
      script : 'yarn start /app/transport-cohort-membership-checker/source',
      cwd: '/app/transport-cohort-membership-checker',
      env: {
        NODE_ENV: 'development',
      },
      env_production: {
        NODE_ENV: 'production',
        NEXT_PUBLIC_TRANSPORT_COHORT_SERVICE_BASE_URL: 'http://149.50.130.26:3000/api/v1',
        PORT: 3001,
      },
    }
  ],
  deploy: {
    production: {
      user: 'deploy',
      host: ['localhost'],
      ref: 'origin/main',
      repo: 'https://gitlab.com/disruptivo.io/clients/transport-cohort/transport-cohort-membership-checker.git',
      path: '/app/transport-cohort-membership-checker',
      'post-setup': 'git submodule update --init --recursive', // run after cloning the repo
      'pre-deploy': 'git pull origin main && git submodule update --recursive && yarn install && yarn build /app/transport-cohort-membership-checker/source',
    },
  },
}