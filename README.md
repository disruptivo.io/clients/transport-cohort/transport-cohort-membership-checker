This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## How to run the project?

1. Clone the project
```bash
git clone --recursive  https://gitlab.com/disruptivo.io/clients/transport-cohort/transport-cohort-membership-checker.git
```
2. Install dependencies
```bash
$ yarn install
```
3. Add the `.env.local` file base on `.env.example` file
4. Run the app
```bash
$ yarn dev
```

## How to pull changes on git submodules?
`git submodule update --recursive --remote`

If it's the first time you check-out a repo you need to use --init first
`git submodule update --init --recursive`

## How to deploy using PM2? (first time)
- Create a .env file based on .env.example file
- Run `yarn deploy:setup`
- Run `yarn deploy`
- Run `pm2 list` to see your deployment

## How to deploy a new release using PM2?
- Run `yarn deploy:update`
- Run if you want to see the current version `yarn deploy:current`
- Run if you want to see the previous version `yarn deploy:previous`
- Run if you want to see all deployments `yarn deploy:list`

## How to revert a deploy with PM2?
- Run `yarn deploy:revert DEPLOY_ID`
