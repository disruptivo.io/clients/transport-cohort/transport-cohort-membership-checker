import './globals.scss'
import type { Metadata } from 'next'
import { Inter } from 'next/font/google'
import Providers from './providers'
import { ToastContainer } from 'react-toastify';
import { version } from '../package.json'
const inter = Inter({ subsets: ['latin'] })

export const metadata: Metadata = {
  title: 'Buscador de Afiliado | Camara de Transportes',
  description: 'Buscador de afilliados de la camara de transportes',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html
      lang='en'
      className='min-h-full h-full'
    >
      <body
        className={`${inter.className} min-h-full h-full`}
      >
        <Providers>
          {children}
          <ToastContainer
            theme='colored'
          />
          <span
            className='text-dark-100 absolute bottom-0 right-0 m-2'
          >
            {version
          }</span>
        </Providers>
      </body>
    </html>
  )
}
