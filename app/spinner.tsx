export default function Spinner({ show }: {
  show: boolean
}) {

  if (!show) return null

  return (
    <div
      className='spinner animate-spin text-accent-100'
    >
      <svg
        viewBox='0 0 100 100'
        className='inline-block w-6 h-6'
      >
        <circle
          cx='50'
          cy='40'
          r='40'
          stroke='currentColor'
          strokeWidth='6'
          fill='none'
        />
      </svg>
    </div>
  )
}