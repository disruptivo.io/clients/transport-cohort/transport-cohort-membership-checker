import moment from 'moment'
import { omit } from 'ramda'

const affiliateEnglishLabelsToSpanish : {
  [key: string]: string
} = {
  id: 'ID',
  code: 'Código',
  name: 'Nombre',
  identityCard: 'Identificación',
  address: 'Dirección',
  phoneNumber: 'Teléfono',
  email: 'Email',
  hasCard: 'Tarjeta',
  status: 'Estado',
  createdAt: 'Fecha de creación',
  updatedAt: 'Fecha de actualización',
  enterpriseId: 'Empresa',
}

const enterpriseEnglishLabelsToSpanish : {
  [key: string]: string
} = {
  id: 'ID',
  name: 'Nombre',
  itin: 'Nit',
  address: 'Dirección',
  phoneNumber: 'Teléfono',
  email: 'Email',
  contactName: 'Nombre de contacto',
  contactPhoneNumber: 'Teléfono de contacto',
  contactEmail: 'Email de contacto',
  type: 'Tipo',
  status: 'Estado',
  subscriptionDueDate: 'Fecha de pago',
  createdAt: 'Fecha de creación',
  updatedAt: 'Fecha de actualización',
}

const fieldsToOmit = [
  'id',
  'createdAt',
  'updatedAt',
]

async function getData({ code } : { code: string }) {
  const query = {
    where: {
      code,
    },
    include: {
      enterprise: true,
    },
  }
  
  const response = await fetch(`${process.env.NEXT_PUBLIC_TRANSPORT_COHORT_SERVICE_BASE_URL}/affiliate/findFirst?q=${JSON.stringify(query)}`)

  if (!response.ok)
    throw new Error('Error al obtener los datos')

  return response.json()
}

function renderTable({ title, data } : { title: string, data: object }) {
  return (
    <div
      className='w-full p-5'
    >
      <h1
        className='text-2xl pb-5'
      >
        {title}
      </h1>
      <div>
        {Object.entries(omit(fieldsToOmit, data)).map(([key, value]) => {
          return (
            <div
              key={key}
              className='odd:bg-light-100 flex flex-row'
            >
              <span className='font-bold md:w-1/3 lg:w-1/5 2xl:w-1/12'>{affiliateEnglishLabelsToSpanish[key] || enterpriseEnglishLabelsToSpanish[key]}:</span><span>{String(moment(String(value), 'YYYY-MM-DDTHH:mm:ss.SSSZ', true).isValid() ? moment(String(value)).format('DD-MM-YYYY') : value)}</span>
            </div>
          )
        })}
      </div>
    </div>
  )
}

export default async function AffiliateDetail({ params: { code }} : { params: { code: string } }) {
  const { data: { enterprise, ...user } } = await getData({ code })
  
  return (
    <>
      {renderTable({ title: 'Afiliado', data: user })}
      {renderTable({ title: 'Empresa', data: enterprise })}
    </>
  )
}