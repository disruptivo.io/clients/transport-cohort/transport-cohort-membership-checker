import type { Metadata } from 'next'

export const metadata: Metadata = {
  title: 'Detalle de Afiliado | Camara de Transporte',
}

export default function AffiliateDetailLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <main
      className='min-h-full h-full'
    >
      {children}
    </main>
  )
}
