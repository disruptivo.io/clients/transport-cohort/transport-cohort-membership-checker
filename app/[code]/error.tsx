'use client'
import styles from  './error.module.scss'

export default function AffiliateDetailError({
  error,
  reset,
}: {
  error: Error
  reset: () => void
}) {

  const onReset = () => {
    reset()
  }
  
  return (
    <div
      className={styles.container}
    >
      <h2>Algo saió mal</h2>
      <button
        className={styles.button}
        onClick={onReset}
      >
        Intentar de nuevo
      </button>
    </div>
  )
}