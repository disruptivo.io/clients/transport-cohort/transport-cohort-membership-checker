"use client"

import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { Provider as ZenStackHooksProvider } from '@/lib/tanstack-query';

const queryClient = new QueryClient();

const Providers = async ({ children }: { children: React.ReactNode }) => {
  return (
    <QueryClientProvider
      client={queryClient}
    >
      <ZenStackHooksProvider
        value={{ endpoint: String(process.env.NEXT_PUBLIC_TRANSPORT_COHORT_SERVICE_BASE_URL) }}
      >
        {children}
      </ZenStackHooksProvider>
    </QueryClientProvider>
  )
}

export default Providers