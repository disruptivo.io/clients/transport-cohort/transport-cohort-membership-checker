"use client"

import { useState, useEffect, useRef } from 'react';
import { useFindFirstAffiliate } from '../lib/tanstack-query';
import Spinner from './spinner';
import CheckIcon from './checkIcon';
import CloseIcon from './closeIcon';
import { useRouter } from 'next/navigation'
import { toast } from 'react-toastify';

export default function Search() {
  const router = useRouter()
  const [affiliateCode, setAffiliateCode] = useState('');
  const timeout: any = useRef(undefined);
  const {
    data: affiliate,
    isFetching,
    isFetched,
    error,
    refetch,
  } = useFindFirstAffiliate({
    where: {
      code: affiliateCode,
    },
    select: {
      code: true,
      status: true,
      enterprise: true,
    },
  }, {
    enabled: false,
  });

  const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (event.target.value)
      setAffiliateCode(event?.target?.value);
  }

  const onGoToAffiliateDetailView = () => {
    router.push(`/${affiliate?.code}`);
  }

  const renderCheckIcon = () => {
    if (!affiliate?.code ||
        affiliate?.status === 'desabilitado' ||
        affiliate?.enterprise?.status === 'desabilitado'
      )
      return null

  return <CheckIcon />
  }

  const renderCloseIcon = () => {
    if (affiliateCode && isFetched && (
        !affiliate?.code ||
        affiliate?.status === 'desabilitado' ||
        affiliate?.enterprise?.status === 'desabilitado')
      )
      return <CloseIcon />

  return null
  }

  const renderLinkToAffiliateDetail = () => {
    if (error || !affiliate?.code)
      return null

    return (
      <button
        className='bg-accent-100 text-light-100 p-2 rounded-md hover:bg-transparent hover:text-accent-100 mt-5'
        type='button'
        onClick={onGoToAffiliateDetailView}
      >
        Ver Detalle
      </button>
    )
  }

  useEffect(() => {
    if (timeout)
      clearTimeout(timeout.current)
    
    timeout.current = setTimeout(() => {
      refetch()
    }, 1000)
  }, [affiliateCode])

  useEffect(() => {
    if (error && error instanceof Error)
      toast.error(String(error.message))
  }, [error])

  return (
      <div
        className='flex flex-col items-center w-full min-w-full'
      >
        <label
          className='text-center'
        >
          Introducir código de afiliado
        </label>
        <input
          className='w-1/2 lg:w-1/4 xl:w-1/6 text-black my-5 rounded-md'
          onChange={onChange}
          type='text'
        />
        <Spinner
          show={isFetching}
        />
        {renderCheckIcon()}
        {renderCloseIcon()}
        {renderLinkToAffiliateDetail()}
      </div>
  )
}