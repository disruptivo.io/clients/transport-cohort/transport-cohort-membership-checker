import styles from  './icon.module.scss'

export default function CheckIcon() {
  return (
    <div
      className={styles.container}
    >
      <svg
        xmlns='http://www.w3.org/2000/svg'
        viewBox='0 0 24 24'
      >
        <path
          className={styles.icon}
          stroke='#86EFAC'
          strokeWidth='1'
          strokeLinejoin='round'
          strokeLinecap='round'
          strokeMiterlimit='10'
          strokeDasharray='340'
          strokeDashoffset='340'
          fill='#86EFAC'
          d='M9,20.42L2.79,14.21L5.62,11.38L9,14.77L18.88,4.88L21.71,7.71L9,20.42Z'
        />
      </svg>
    </div>
  )
}