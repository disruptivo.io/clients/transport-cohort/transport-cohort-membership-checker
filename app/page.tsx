import Search from './search';
import Logo from './logo';

export default function Home() {
  return (
    <main
      className='h-full flex flex-col items-center p-5'
    >
      <Logo />
      <Search />
    </main>
  )
}
