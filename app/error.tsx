'use client'
import styles from  './error.module.scss'

export default function RootError({
  error,
  reset,
}: {
  error: Error
  reset: () => void
}) {

  const onReset = () => {
    reset()
  }
  
  return (
    <html
      className={styles.root}
    >
      <body
        className={styles.container}
      >
        <h2>Algo saió mal</h2>
        <button
          className={styles.button}
          onClick={onReset}
        >
          Intentar de nuevo
        </button>
      </body>
    </html>
  )
}