/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');

module.exports = {
  content: [
    './pages/**/*.{js,ts,jsx,tsx,mdx}',
    './components/**/*.{js,ts,jsx,tsx,mdx}',
    './app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      backgroundImage: {
        'gradient-radial': 'radial-gradient(var(--tw-gradient-stops))',
        'gradient-conic':
          'conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))',
      },
    },
    minWidth: {
      '1/5': '20%',
      'full': '100%',
    },
    colors: {
      ...colors,
      'primary': '#F7F7F2',
      'primary-light': '#F1F9F5',
      'primary-dark': '#132A20',
      'accent-100': '#285943',
      'light-100': '#FFFFFF',
      'dark-100': '#343434',
      'error': '#F8A5A5',
      'success': '#86EFAC',
    },

  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
